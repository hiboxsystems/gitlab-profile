Hibox Systems is a global provider of advanced interactive information and entertainment solutions for consumers and commercial applications. We are headquartered in Turku, Finland and a part of the [Accedo Group](https://www.accedo.tv)

https://www.hibox.tv